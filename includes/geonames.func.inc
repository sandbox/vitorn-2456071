<?php

function geonames_get_elements_form() {
  $form = array();

  $form['countries'] = array(
    '#type' => 'geonames_countries_list',
    '#load_states' => TRUE,
  );

  $form['states'] = array(
    '#type' => 'geonames_states_list',
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Testing!'
  );
  return $form;
}

function geonames_get_elements_form_submit($form, $form_state) {
  var_dump($form_state); die;
}

function _geonames_load_countries_list() {
    $url = _geonames_get_webservice_url(WEBSERVICE_URL_GET_COUNTRIES);
    $ws = new Webservice();
    $result = $ws->get($url);
    $countries = json_decode($result);

    $countries_select = array();

    /** Loop for mount and assoc array for select box */
    if ($countries && isset($countries->geonames)) {
        foreach($countries->geonames as $country) {
            $countries_select += array(
                $country->countryName => $country->countryName
            );
        }
        /** Order array by values */
        asort($countries_select);
    }
    return $countries_select;
}

function _geonames_load_state_list($country) {
  $url = _geonames_get_webservice_url(WEBSERVICE_URL_GET_STATES, $country);
  $ws = new Webservice();
  $result = $ws->get($url);
  $states = json_decode($result);

  $states_select = array();

  /** Loop for mount and assoc array for select box */
  if ($states && isset($states->geonames)) {
    foreach($states->geonames as $state) {
      $states_select += array(
        $state->toponymName => $state->toponymName
      );
    }
    /** Order array by values */
    asort($states_select);
  }
  return $states_select;
}

/** Function to generate the Webservice URL */
function _geonames_get_webservice_url($url, $country = NULL) {
  $username = variable_get('geonames_username');
  $lang = variable_get('geonames_language');

  if (!is_null($country)) {
    $url = str_replace('[country]', urlencode($country), $url);
  }

  /** Replace the placeholders */
  $url = str_replace('[username]', urlencode($username), $url);
  $url = str_replace('[lang]', $lang, $url);
  return $url;
}