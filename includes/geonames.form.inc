<?php
/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 3/21/15
 * Time: 12:18 AM
 */

function _geonames_countries_list_attributes_process($element, &$form_state) {
  /** Get some attrs of the field */
  $field_name = $element['#name'];
  $field_title = isset($element['#title']) ? $element['#title'] : '';

  $default_value = !is_array($form_state['values'][$field_name]) &&
  !empty($form_state['values'][$field_name]) ? $form_state['values'][$field_name] :
    urlencode(variable_get('geonames_default_country','Brazil'));

  $element['country']['#type'] = 'select';
  $element['country']['#default_value'] = $default_value;
  $element['country']['#title'] = $field_title;
  $element['country']['#options'] = _geonames_load_countries_list();
  $element['country']['#prefix'] = '<div class="geonames-country-wrapper">';
  $element['country']['#suffix'] = '</div>';
  $element['country']['#name'] = $element['#name'];

  /** Before to configure ajax here, we must to know, if this is necessary
   * Because the user just want the country select box
   * So, if #load_states is TRUE, lets put the AJAX there.
   */
  if (isset($element['#load_states']) && $element['#load_states']) {
    $element['country']['#ajax'] = array(
      'event' => 'change',
      'callback' => '_geonames_load_states_from_country_ajax_callback',
      'wrapper' => 'geonames-state-wrapper',
    );
  }
  return $element;
}

function _geonames_states_list_attributes_process($element, &$form_state) {
  /** Before to set the element, lets keep some info defined
   * on the moment where the form was created */

  /** If the user wants only the states from a country
   * This country must came on element array
   */
  if (isset($element['#country'])) {
    $selected = $element['#country'];
  } else {
    $selected = !is_array($form_state['values']['countries']) &&
    !empty($form_state['values']['countries']) ? $form_state['values']['countries'] :
      urlencode(variable_get('geonames_default_country','Brazil'));
  }

  /** save the name of field on session to know which name the field has
   * To call on AJAX request
   */
  $_SESSION['geonames_state_field_name'] = $element['#name'];

  /** Create the rest of element */
  $element['state']['#type'] = 'select';
  $element['state']['#name'] = $element['#name'];
  $element['state']['#default_value'] = $form_state['values']['states'];
  $element['state']['#options'] = _geonames_load_state_list($selected);
  $element['state']['#prefix'] = '<div id="geonames-state-wrapper">';
  $element['state']['#suffix'] = '</div>';
  return $element;
}

/** AJAX callback function when change the country */

function _geonames_load_states_from_country_ajax_callback($form, &$form_state) {
  /** @var $field
   * get the field name from the session
   */
  $field = $_SESSION['geonames_state_field_name'];

  /** Now, return the element */
  return $form[$field]['state'];
}