<?php
/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 3/21/15
 * Time: 12:39 AM
 */

function _geonames_get_includes_inc_files ()
{
  //Default files
  module_load_include('inc', 'geonames', 'includes/geonames.admin');
  module_load_include('inc', 'geonames', 'includes/geonames.form');
  module_load_include('inc', 'geonames', 'includes/geonames.field');
  module_load_include('inc', 'geonames', 'includes/geonames.func');
}