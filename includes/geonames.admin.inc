<?php
/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 3/21/15
 * Time: 11:07 AM
 */

/** Function to return the hook_menu from admin area  */
function geonames_get_admin_menu(&$items) {
  $items['admin/config/regional/geonames'] = array(
    'title' => 'Geonames',
    'description' => 'Configure the Geonames module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('geonames_admin_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/** function to return the configuration form */
function geonames_admin_form() {
  $form = array();

  $form['geonames_username'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('geonames_username', 'demo'),
    '#title' => t('Username'),
    '#description' => t('The username that will be used Webservice request.'),
    '#size' => 20
  );

  $form['geonames_language'] = array(
    '#type' => 'select',
    '#options' => array('en' => t('English'), 'de' => t('Deutsch')),
    '#default_value' => variable_get('geonames_language', 'en'),
    '#title' => t('Language'),
    '#description' => t('Language will be used on Webservice request.'),
  );

  $form['geonames_default_country'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('geonames_default_country', 'Brazil'),
    '#title' => t('Default Country'),
    '#description' => t('Default country will be used on Webservice request When there is no country defined'),
  );

  $form = system_settings_form($form);
  return $form;
}