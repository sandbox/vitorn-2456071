<?php

/** Create a function to return an array with the field infos  */
function _geonames_get_field_info(&$fields) {
  $fields['country'] = array(
    'label' => t('Country'),
    'description' => t('This field stores a country in the database.'),
    'settings' => array(),
    'instance_settings' => array(),
    'default_widget' => 'country_select',
    'default_formatter' => 'geonames_default',
  );

  $fields['state'] = array(
    'label' => t('State/Province'),
    'description' => t('This field stores a state/province in the database.'),
    'settings' => array(),
    'instance_settings' => array(),
    'default_widget' => 'states_select',
    'default_formatter' => 'geonames_default',
  );
}

/** Function to get the widget types */
function _geonames_get_field_widget_type_info(&$widgets) {

  $widgets['country_select'] = array(
    'label' => t('Dynamic country Select Box'),
    'field types' => array('country'),
  );

  $widgets['select_select'] = array(
    'label' => t('Dynamic State Select Box'),
    'field types' => array('state'),
  );
}